# Packer Bio-Linux 8 image

This project includes scripts to build a Bio-Linux 8 image suitable for the NeCTAR Research Cloud environment.

We have:

* Packer JSON config for building the image on the NeCTAR Research Cloud.

## Requirements

You'll require the following tools installed and in your path

* Packer
* OpenStack CLI
* jq (JSON CLI tool)
* QEMU tools (for image shrinking process)

## Building the image

1. Make sure all the required software (listed above) is installed
1. Load your NeCTAR RC credentials into your environment
1. cd to the directory containing this README.md file
1. The build script

```
./build_local.sh
```