#!/bin/bash -eux

# get first group id that is greater than 100
function get_gid()
{
  grp_file=/etc/group
  min_gid=100
  for i in $(cut -d : -f 3 $grp_file |sort -n) ; do
    if [ $i -gt $min_gid ] ; then
      if [ $((i-min_gid)) -gt 1 ] ; then
        echo $((min_gid+1))
        break
      else
        min_gid=$i
      fi
    fi
  done
}

# save patch file with needed fixes for upgrade script
function put_upgrade_patch()
{
  file=$1
  echo $file
  cat << EOF > $file
--- upgrade_to_8.sh
+++ upgrade_to_8.sh
@@ -183,7 +183,7 @@
 cmirrr="\`infer_cran_mirror\`"
 cat >/etc/apt/sources.list.d/cran-latest-r.list <<.
 #Latest R-cran packages
-deb \$cmirrr/bin/linux/ubuntu trusty/
+deb [trusted=yes] \$cmirrr/bin/linux/ubuntu trusty/
 deb-src \$cmirrr/bin/linux/ubuntu trusty/
 .
 
@@ -208,7 +208,7 @@
 cat >/etc/apt/sources.list.d/google-chrome.list <<"."
 ### THIS FILE IS AUTOMATICALLY CONFIGURED ###
 # You may comment out this entry, but any other modifications may be lost.
-deb http://dl.google.com/linux/chrome/deb/ stable main
+deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main
 .
 cat >/etc/apt/sources.list.d/google-talkplugin.list <<"."
 ### THIS FILE IS AUTOMATICALLY CONFIGURED ###
@@ -239,8 +239,8 @@
 
 # If this was run on a Vanilla Ubuntu 14.04 box then Universe/Multiverse sources
 # will not be active.  Tell the user about it.
-if grep -q '^deb .*/ [a-z ]\+ universe$'   /etc/apt/sources.list && \\
-   grep -q '^deb .*/ [a-z ]\+ multiverse$' /etc/apt/sources.list ; then
+if grep -q '^deb [^ ]* [a-z ]\+ universe$'   /etc/apt/sources.list && \\
+   grep -q '^deb [^ ]* [a-z ]\+ multiverse$' /etc/apt/sources.list ; then
     true
 else
     echo "**** Warning:"
EOF
}

put_apt_conf()
{
  file=$1
  cat << EOF > $file
Dpkg::Options {
  "--force-confdef";
  "--force-confold";
}
EOF
}

bl_user=ubuntu
bl_uname=$(getent passwd $bl_user | cut -d : -f 5)
bl_uhome=$(getent passwd $bl_user | cut -d : -f 6)

# set up arb group and user to avoid install prompt
gid=$(get_gid)
groupadd -g $gid arb
usermod -aG arb $bl_user
debconf-set-selections <<< "arb-common arb/group multiselect $bl_user ($bl_uname)"

# get, patch, and run bio-linux-8 upgrade script
wget -nv -P /tmp http://nebc.nerc.ac.uk/downloads/bl8_only/upgrade8.sh
export DEBIAN_FRONTEND="noninteractive"
apt-get update
apt-get -y install firefox libreoffice
export UNPACK_ONLY=1
chmod +x /tmp/upgrade8.sh
upgrade_path=$(/tmp/upgrade8.sh 2>&1 | sed -n 's/^Unpack complete in \(.*\)\. .*$/\1/p')
patch_file="/tmp/upgrade_to_8.sh.patch"
put_upgrade_patch $patch_file
conf_file="/etc/apt/apt.conf.d/000_biolinux_tmp"
put_apt_conf $conf_file
patch $upgrade_path/upgrade_to_8.sh $patch_file
cwd=$(pwd)
cd $upgrade_path
./upgrade_to_8.sh
cd $cwd
# bio-linux sets keyboard layout to GB - remove/comment out
sed  -i '/^\[org\/mate\/desktop\/peripherals\/keyboard\/kbd\]$/,/^[\[].*$/ {
  /^\[org\/mate\/desktop\/peripherals\/keyboard\/kbd\]$/b in
  /^[\[].*$/b out
  :in s/^/#/;
  :out
}' /etc/dconf/db/site.d/21_mate-defaults
dconf update
# work-around for bug in python-future/trusty,now 0.14.3-0biolinux5
# that breaks urllib3 and cloud-init
# see https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=796356
wget -nv -P /tmp https://launchpad.net/ubuntu/+archive/primary/+files/python-future_0.15.2-1_all.deb
sudo dpkg -i /tmp/python-future_0.15.2-1_all.deb
