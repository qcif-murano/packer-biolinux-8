#!/bin/bash -eux

# Delete apt conf needed for bio-linux install
conf_file="/etc/apt/apt.conf.d/000_biolinux_tmp"
rm $conf_file

# Clean up Ubuntu user
userdel -rf ubuntu || true

# Zero out the rest of the free space using dd, then delete the written file.
dd if=/dev/zero of=/EMPTY bs=1M
rm -f /EMPTY

# Add `sync` so Packer doesn't quit too early, before the large file is deleted.
sync
